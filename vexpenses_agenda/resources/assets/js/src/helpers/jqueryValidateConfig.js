// Set jQuery.validate settings for bootstrap integration
module.exports = {
    highlight: function(element) {
      jQuery(element).closest('.form-group').addClass('has-error');
      //jQuery(element).closest('.form-checkbox').addClass('has-error');
    },
    unhighlight: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-error');
      //jQuery(element).closest('.form-checkbox').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    ignore: ':hidden:not([class~=selectized]):not(.js-validate-hidden),:hidden > .selectized, .selectize-control .selectize-input input',
    errorPlacement: function(error, element) {
  
      if (element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else if(element.parent('.form-group').length) {
        error.appendTo(element.parent('.form-group'));
      //} else if(element.parent('.form-checkbox').length) {
        //error.appendTo(element.parent('.form-checkbox'));
      } else {
        error.insertAfter(element);
      }
    }
  };