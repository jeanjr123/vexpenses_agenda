const format_date = (european_date) => {
    const trimed_date = european_date.replace(/^\s+/g, '').replace(/\s+$/g, '');
    if (trimed_date != '') {
      const frDatea = trimed_date.split(' ');
      const frTimea = frDatea[1].split(':');
      const frDatea2 = frDatea[0].split('/');
  
      return (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
    } else {
      return 10000000000000;
    }
  }
  
  jQuery.fn.dataTableExt.oSort['date-euro-asc'] = function (x, y) {
    x = format_date(x);
    y = format_date(y);
  
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
  };
  
  jQuery.fn.dataTableExt.oSort['date-euro-desc'] = function (x, y) {
    x = format_date(x);
    y = format_date(y);
  
    return ((x < y) ? 1 : ((x > y) ? -1 : 0));
  };
  
  module.exports = format_date;
  