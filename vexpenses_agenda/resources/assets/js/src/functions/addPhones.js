exports.init = () => {
  $(document).on('click','#addPhones',function(){
    let numPhones = this.value;
    numPhones++;
    $('#morePhones .morePhones').append(
    '<div class="form-group row"    id="phones'+numPhones+'">\
     <div class="col-md-6">\
     <label>Celular</label>\
     <input type="text" name="mobile_phone[]"  class="form-control">\
     </div>\
     <div class="col-md-6">\
     <label>Telefone</label>\
     <input type="text" name="phone[]" class="form-control">\
     </div>\
     </div>'
     );

    $('#rmPhonesDiv').remove();

    $('#btnsPhone').append(
    '<div class="btn-group" id="rmPhonesDiv">\
     <button class="btn btn-danger" id="rmPhones" value="'+numPhones+'">\
     <i class="fa fa-minus"></i>\
     Remover Telefone\
     </button>\
     </div>');

    $('#addPhones').val(numPhones);

    return false;
  });
}