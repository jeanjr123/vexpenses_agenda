
const validateForms = require('./validateForms');
const cepCreateContact = require('./cepCreateContact');




exports.init = () => {
  $('button.open-modal-js').click((e) => {
    $.ajax({
      url: e.currentTarget.dataset.href,
      type: 'GET',
      success: function (el) {
        console.log(el);
        $(e.currentTarget.dataset.targetContent).replaceWith(el);
        cepCreateContact.init();
        validateForms.init();
      }
    })
  });
}
