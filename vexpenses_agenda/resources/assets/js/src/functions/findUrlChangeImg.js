exports.init = () => {
  $('button.open-modal-photo-js').click((e) => {
    console.log(e.currentTarget.dataset.targetContent);
    $.ajax({
      url: e.currentTarget.dataset.href,
      type: 'GET',
      success: function (el) {
        $(e.currentTarget.dataset.targetContent)
        .attr(e.currentTarget.dataset.targetAttr,'http://'+window.location.host+'/storage/'+el);
      }
    })
  });
}
