const language = require('../helpers/DataTablesConfig.js');
//const formatDate = require('../helpers/FormatDateDatatable');

function MakeDataTableOnEach() {
  const dataTableSelector = $('.data-table');
  const order = $('[data-orderby]');
  if (dataTableSelector.length) {
    dataTableSelector.each(function(n, el) {
      const options = {};
      if (!el.classList.contains('dataTable')) {
        if (order.attr('format-date')) {
          options.columnDefs = [
            {
              type:'date-euro', targets: parseInt(order.attr('format-date'))
            }
          ]
        }
        if (el.dataset.checkboxTable == 'true') {
          options.columnDefs = [{
            'targets': 0,
            'orderable': false
          }]
        } else if(el.classList.contains('options')) {
          options.columnDefs = [{
            'targets': (dataTableSelector.find('tr')[0].cells.length) - 1,
            'orderable': false
          }]
        }
        if(order.attr('data-orderby')) {
          options.order = [
            [order.attr('data-orderby'), 'desc']
          ]
        }
        options.drawCallback = () => {
          $('.add-tooltip').tooltip();
        }
        options.sDom = '<f<t>pi>';
        options.paging = !el.classList.contains('no-paging');
        options.info = !el.classList.contains('no-info');
        options.filter = !el.classList.contains('no-filter');
        options.lengthChange = !el.classList.contains('no-length');
        options.language = language('pt-BR').language;
        $(el).DataTable(options);
      }
    });
  }
}

module.exports = MakeDataTableOnEach;
