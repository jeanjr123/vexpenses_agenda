const Flatpicker = require('flatpickr');
const pt = require('flatpickr/dist/l10n/pt.js');
const moment = require('moment');

function MakeDatePickerOnEach() {
  new Flatpicker('[data-input=datetimepicker]', {
    locale: 'pt',
    dateFormat: 'd/m/Y H:i:S',
    enableTime: true,
    time_24hr: true,
    static: true,
    parseDate: function (str) {
      return moment(str, 'YYYY-MM-DD +-HH:mm:ss').utc().toDate();
    }
  });

  new Flatpicker('[data-input=datepicker]', {
    locale: 'pt',
    dateFormat: 'd/m/Y'
  });

  new Flatpicker('[data-input=timepicker]', {
    locale: 'pt',
    dateFormat: 'H:i',
    enableTime: true,
    noCalendar: true,
    time_24hr: true
  });
}

module.exports = MakeDatePickerOnEach;
