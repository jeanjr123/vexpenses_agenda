exports.init = () => {
  $(document).on('click','#rmAddress',function(){
    let numAddress = this.value;
    $(`#address${numAddress}`).remove();

    $(`#separate${numAddress}`).remove();

    numAddress--;

    $('#rmAddress').val(numAddress);

    return false;

  });
}