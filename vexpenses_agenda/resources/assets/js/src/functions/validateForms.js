const jqueryValidateConfig = require('../helpers/jqueryValidateConfig');

exports.init = function() {
  const formSelector = $('form[data-validate]');
  const options = {};

  jQuery.validator.setDefaults(jqueryValidateConfig);

  /** campos obrigatórios */
  jQuery.validator.addMethod('notEmpty', function (value, element) {
    return value.trim().length > 0;
  }, 'Campo obrigatório');


  /** imagem obrigatoria preview*/
  jQuery.validator.addMethod('fileEmpty', function(value, element) {
    const $img = $(element.dataset.previewImg);

    if (element.files[0] == undefined) {
      $img[0].style.border = '1px solid #a94442';
      return false;
    }

    $img[0].style.border = '1px solid #ddd';
    return true;

  }, 'Selecione uma imagem!');

  /** imagem obrigatoria */
  jQuery.validator.addMethod('fileNotEmpty', function (value, element) {
    if (element.files[0] == undefined) {
      return false;
    }

    return true;

  }, 'Selecione uma imagem!');

  jQuery.validator.addMethod('one', function() {
    const $target = $('input[data-rule-one="true"], select[data-rule-one="true"]');
    let fails = 0;
    $target.each(function(n, el) {
      if($(el).is(':blank')) {
        fails++;
      }
    });
    return fails < $target.length;
  }, 'Pelo menos um destes campos precisa ser preenchido!');

  if (formSelector.length) {
    formSelector.each(function(n, el) {
      if (!el.classList.contains('js-validating')) {
        $(el).validate(options);
        el.classList.add('js-validating');
      }
    });
  }
}