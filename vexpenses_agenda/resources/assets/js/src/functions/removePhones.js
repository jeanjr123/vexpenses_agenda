exports.init = () => {
  $(document).on('click','#rmPhones',function(){
    let numPhones = this.value;
    $(`#phones${numPhones}`).remove();
    numPhones--;
    $('#rmPhones').val(numPhones);
    return false;
  });
}