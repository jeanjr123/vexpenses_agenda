function MakeSelectize(id) {
  const select = $(id);
  const selected = select.data('selected');
  const paddingBottom = select.data('paddingBottom');
  if (!select.length) {
    return select;
  }
  const phrase = 'Sem dados para selecionar...' || phrase;
  const canAdd = false || select.data('selectize-can-add');
  const max = false || select.data('selectize-max-items');
  const selectize = select.selectize({
    plugins: ['restore_on_backspace', 'remove_button'],
    create: canAdd,
    maxItems: max,
    selectOnTab: true,
    hideSelected: true,
    onChange: function() {
      const event = new Event('change', {bubbles: true});
      this.$input[0].dispatchEvent(event);
      try {
        this.$input.valid();
      } catch (e) {

      }
    }
  });

  const $selectize = selectize[0].selectize;

  if ($selectize.options < 1) {
    $selectize.settings.placeholder = phrase;
    $selectize.updatePlaceholder();
  }

  if (selected) {
    $selectize.setValue(selected, true);
  }

  $selectize.on('dropdown_open', function($dropdown) {
    const dropdownSize = $dropdown.offset().top + $dropdown.outerHeight();
    const bodySize = $('body').height();
    if(dropdownSize > bodySize) {
      $('.content-wrapper').css('padding-bottom', dropdownSize - bodySize + 15);
    }
  });
  $selectize.on('dropdown_close', function() {
    $('.content-wrapper').css('padding-bottom', '');
  });

  return $selectize;
};

module.exports = MakeSelectize;
