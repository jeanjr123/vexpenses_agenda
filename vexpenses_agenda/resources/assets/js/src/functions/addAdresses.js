exports.init = () => {
  $(document).on('click','#add',function(){
    let numAddress = this.value;
    numAddress++;

    $('#moreAddress .moreAddress').append(
        '<hr id="separate'+numAddress+'">\
         <div id="address'+numAddress+'">\
         <div class="form-group row">\
         <div class="col-md-2">\
         <label>Cep</label>\
         <input type="text" name="cep[]" class="cep form-control" id="'+numAddress+'">\
         </div>\
         <div class="col-md-4">\
         <label>Rua</label>\
         <input type="text" name="street[]" id="street'+numAddress+'" class="form-control">\
         </div>\
         <div class="col-md-2">\
         <label>Número</label>\
         <input type="text" name="number[]" class="form-control">\
         </div>\
         <div class="col-md-4">\
         <label>Bairro</label>\
         <input type="text" name="neighborhood[]" id="neighborhood'+numAddress+'" class="form-control">\
         </div>\
         </div>\
         <div class="form-group row">\
         <div class="col-md-4">\
         <label>Cidade</label>\
         <input type="text" name="city[]"  id="city'+numAddress+'" class="form-control">\
         </div>\
         <div class="col-md-2">\
         <label>Estado</label>\
         <input type="text" name="state[]" id="state'+numAddress+'" class="form-control">\
         </div>\
         </div>\
         ');

    $('#rmAddressDiv').remove();

    $('#btnsAddress').append(
            '<div class="btn-group" id="rmAddressDiv">\
            <button class="btn btn-danger" id="rmAddress" value="'+numAddress+'">\
             <i class="fa fa-minus"></i>\
             Remover Endereço\
            </button>\
            </div>');

    $('#add').val(numAddress);

    return false;
  });

}