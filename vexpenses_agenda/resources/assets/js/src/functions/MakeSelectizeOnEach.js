const MakeSelectize = require('./MakeSelectize.js');

function MakeSelectizeOnEach() {
  const $selects = $('[data-input=selectize]');
  if ($selects.length) {
    $selects.each(function(n, el) {
      if (!el.selectize && el.tagName.toLowerCase() === 'select') {
        MakeSelectize(el);
      }
    });
  }
}

module.exports = MakeSelectizeOnEach;
