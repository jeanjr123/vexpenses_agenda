const cepCreateContact = require('./functions/cepCreateContact');
const findUrl = require('./functions/findUrl');
const TableLink = require('table-link');
const findUrlChangeImg = require('./functions/findUrlChangeImg');
const addPhones = require('./functions/addPhones');
const removePhones = require('./functions/removePhones');
const addAdresses = require('./functions/addAdresses');
const removeAdresses = require('./functions/removeAdresses');
const validateForms = require('./functions/validateForms');
const MakeDataTableOnEach = require('./functions/MakeDataTableOnEach');
const MakeSelectizeOnEach = require('./functions/MakeSelectizeOnEach');
const MakeDatePickerOnEach = require('./functions/MakeDatePickerOnEach');
const InputMask = require('./functions/InputMask');

MakeSelectizeOnEach();
MakeDatePickerOnEach();
MakeDataTableOnEach();

TableLink.init();
cepCreateContact.init();
findUrl.init();
findUrlChangeImg.init();
addPhones.init();
removePhones.init();
addAdresses.init();
removeAdresses.init();
validateForms.init();
InputMask.init();


