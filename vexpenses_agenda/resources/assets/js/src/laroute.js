(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://localhost',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"api\/user","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"login","name":"login","action":"App\Http\Controllers\Auth\LoginController@showLoginForm"},{"host":null,"methods":["POST"],"uri":"login","name":null,"action":"App\Http\Controllers\Auth\LoginController@login"},{"host":null,"methods":["POST"],"uri":"logout","name":"logout","action":"App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"register","name":"register","action":"App\Http\Controllers\Auth\RegisterController@showRegistrationForm"},{"host":null,"methods":["POST"],"uri":"register","name":null,"action":"App\Http\Controllers\Auth\RegisterController@register"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset","name":"password.request","action":"App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm"},{"host":null,"methods":["POST"],"uri":"password\/email","name":"password.email","action":"App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset\/{token}","name":"password.reset","action":"App\Http\Controllers\Auth\ResetPasswordController@showResetForm"},{"host":null,"methods":["POST"],"uri":"password\/reset","name":"password.update","action":"App\Http\Controllers\Auth\ResetPasswordController@reset"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/confirm","name":"password.confirm","action":"App\Http\Controllers\Auth\ConfirmPasswordController@showConfirmForm"},{"host":null,"methods":["POST"],"uri":"password\/confirm","name":null,"action":"App\Http\Controllers\Auth\ConfirmPasswordController@confirm"},{"host":null,"methods":["GET","HEAD"],"uri":"home","name":"home","action":"App\Http\Controllers\HomeController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/categories","name":"schedule.categories.index","action":"App\Http\Controllers\Schedule\CategoryController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/categories\/create","name":"schedule.categories.create","action":"App\Http\Controllers\Schedule\CategoryController@create"},{"host":null,"methods":["POST"],"uri":"schedule\/categories","name":"schedule.categories.store","action":"App\Http\Controllers\Schedule\CategoryController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/categories\/{category}","name":"schedule.categories.show","action":"App\Http\Controllers\Schedule\CategoryController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/categories\/{category}\/edit","name":"schedule.categories.edit","action":"App\Http\Controllers\Schedule\CategoryController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"schedule\/categories\/{category}","name":"schedule.categories.update","action":"App\Http\Controllers\Schedule\CategoryController@update"},{"host":null,"methods":["DELETE"],"uri":"schedule\/categories\/{category}","name":"schedule.categories.destroy","action":"App\Http\Controllers\Schedule\CategoryController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/phones","name":"schedule.phones.index","action":"App\Http\Controllers\Schedule\PhoneController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/phones\/create","name":"schedule.phones.create","action":"App\Http\Controllers\Schedule\PhoneController@create"},{"host":null,"methods":["POST"],"uri":"schedule\/phones","name":"schedule.phones.store","action":"App\Http\Controllers\Schedule\PhoneController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/phones\/{phone}","name":"schedule.phones.show","action":"App\Http\Controllers\Schedule\PhoneController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/phones\/{phone}\/edit","name":"schedule.phones.edit","action":"App\Http\Controllers\Schedule\PhoneController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"schedule\/phones\/{phone}","name":"schedule.phones.update","action":"App\Http\Controllers\Schedule\PhoneController@update"},{"host":null,"methods":["DELETE"],"uri":"schedule\/phones\/{phone}","name":"schedule.phones.destroy","action":"App\Http\Controllers\Schedule\PhoneController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/adresses","name":"schedule.adresses.index","action":"App\Http\Controllers\Schedule\AddressController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/adresses\/create","name":"schedule.adresses.create","action":"App\Http\Controllers\Schedule\AddressController@create"},{"host":null,"methods":["POST"],"uri":"schedule\/adresses","name":"schedule.adresses.store","action":"App\Http\Controllers\Schedule\AddressController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/adresses\/{adress}","name":"schedule.adresses.show","action":"App\Http\Controllers\Schedule\AddressController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/adresses\/{adress}\/edit","name":"schedule.adresses.edit","action":"App\Http\Controllers\Schedule\AddressController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"schedule\/adresses\/{adress}","name":"schedule.adresses.update","action":"App\Http\Controllers\Schedule\AddressController@update"},{"host":null,"methods":["DELETE"],"uri":"schedule\/adresses\/{adress}","name":"schedule.adresses.destroy","action":"App\Http\Controllers\Schedule\AddressController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/contacts","name":"schedule.contacts.index","action":"App\Http\Controllers\Schedule\ContactController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/contacts\/create","name":"schedule.contacts.create","action":"App\Http\Controllers\Schedule\ContactController@create"},{"host":null,"methods":["POST"],"uri":"schedule\/contacts","name":"schedule.contacts.store","action":"App\Http\Controllers\Schedule\ContactController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/contacts\/{contact}","name":"schedule.contacts.show","action":"App\Http\Controllers\Schedule\ContactController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/contacts\/{contact}\/edit","name":"schedule.contacts.edit","action":"App\Http\Controllers\Schedule\ContactController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"schedule\/contacts\/{contact}","name":"schedule.contacts.update","action":"App\Http\Controllers\Schedule\ContactController@update"},{"host":null,"methods":["DELETE"],"uri":"schedule\/contacts\/{contact}","name":"schedule.contacts.destroy","action":"App\Http\Controllers\Schedule\ContactController@destroy"},{"host":null,"methods":["POST"],"uri":"schedule\/photos\/remove\/{contactId}","name":"schedule.photo.remove","action":"App\Http\Controllers\Schedule\ContactController@removePhoto"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/exports","name":"schedule.contacts.export","action":"App\Http\Controllers\Schedule\ContactController@exportContacts"},{"host":null,"methods":["GET","HEAD"],"uri":"schedule\/print","name":"schedule.contacts.print","action":"App\Http\Controllers\Schedule\ContactController@printPdf"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);

