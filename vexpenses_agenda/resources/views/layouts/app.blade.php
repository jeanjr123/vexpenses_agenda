<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Agenda Vexpenses</title>

    <!-- Scripts -->
     
 
     
     

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/app.min.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" 
    integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" 
    crossorigin="anonymous">
    <script src="{{asset('assets/js/libs.js')}}"></script>

   
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Agenda Vexpenses
                </a>
               
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @auth
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item @if(request()->is('schedule/contacts*')) active @endif">
                            <a class="nav-link" href={{route('schedule.contacts.index')}}>Contatos<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item @if(request()->is('schedule/categories*')) active @endif">
                            <a class="nav-link" href={{route('schedule.categories.index')}}>Categorias<span class="sr-only">(current)</span></a>
                        </li>

                    </ul>
                    @endauth
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        <ul class="navbar-nav mr-auto">
                             
                            <li class="nav-item ">
                              <a class="nav-link" href="#"; onclick="event.preventDefault(); document.querySelector('form.logout').submit();">Sair</a>
                              
                               <form action="{{route('logout')}}" class="logout" method="POST" style="display:none">
                                    @csrf
                        
                               </form>
                            </li>
                            <li class="nav-item">
                                <span class="nav-link">
                                    {{auth()->user()->name}}
                                </span>
                            </li>

                        </ul>
                        @endguest
                 
                </div>
            </div>
        </nav>
        <main class="py-4">
            <div class="container">
                @include('flash::message')
                @yield('content')
            </div>
        </main>
    </div>
   
    <script src="{{asset('assets/js/app.js') }}"></script>
</body>
</html>
