<?php
use Illuminate\Support\Collection;

Html::macro('menuItem', function ($name, $url, $urlActive, $icon = false, $except = null) {
    $element = '<li class="' . active($urlActive, 'active', $except) . '">';
    $element .= '<a href="' . $url . '">';
    if ($icon) {
        $element .= '<i class="' . $icon . '"></i>';
    };
    $element .= '<span class="menu-title">' . $name . '</span>';
    $element .= '</a>';
    $element .= '</li>';

    return $element;
});

Html::macro('menuItemCollapse', function ($name, $url, $urlActive, $icon = false) {
    $element = '<li class="' . active($urlActive) . '">';
    $element .= '<a href="' . $url . '">';
    if ($icon) {
        $element .= '<i class="' . $icon . '"></i>';
    };
    $element .= '<span class="menu-title">' . $name . '</span>';
    $element .= '</a>';
    $element .= '</li>';

    return $element;
});

Form::macro('textField', function ($name, $label = NULL, $value = NULL, $attributes = []) {
    $element = Form::text($name, $value ? $value : old($name), field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element);
});

Form::macro('numberField', function ($name, $label = NULL, $value = NULL, $attributes = []) {
    $element = Form::number($name, $value ? $value : old($name), field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element);
});

Form::macro('textAreaField', function ($name, $label = null, $value = null, $attributes = []) {
    $element = Form::textarea($name, $value ? $value : old($name), field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element);
});

Form::macro('selectField', function ($name, $label = NULL, $options = [], $value = NULL, $attributes = []) {
    $element = Form::select($name, $options, $value ? $value : old($name), field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element);
});

Form::macro('dateField', function ($name, $label = NULL, $value = NULL, $attributes = []) {
    $element = '<div class="input-group date">';
    $element .= Form::dateTime($name, $value ? $value : old($name), field_attributes($name, $attributes));
    $element .= '<span class="input-group-addon">';
    $element .= '<i class="fa fa-calendar"></i>';
    $element .= '</span>';
    $element .= '</div>';

    return field_wrapper($name, $label, $element);
});

Form::macro('dateFieldIcon', function ($name, $label = null, $value = null, $attributes = []) {
    $element = Form::dateTime($name, $value ? $value : old($name), field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element);
});

Form::macro('timeField', function ($name, $label = NULL, $value = NULL, $attributes = []) {
    $element = '<div class="input-group date">';
    $element .= Form::text($name, $value ? $value : old($name), field_attributes($name, $attributes));
    $element .= '<span class="input-group-addon">';
    $element .= '<i class="fa fa-clock-o"></i>';
    $element .= '</span>';
    $element .= '</div>';

    return field_wrapper($name, $label, $element);
});

Form::macro('formGroupText', function ($name, $label = NULL, $value = NULL, $icon = NULL, $attributes = []) {
    $element = Form::text($name, $value ? $value : old($name), field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element, $icon);
});

Form::macro('formGroupPassword', function ($name, $label = NULL, $icon = NULL, $attributes = []) {
    $element = Form::password($name, field_attributes($name, $attributes));

    return field_wrapper($name, $label, $element, $icon);
});

Form::macro('checkboxField', function ($name, $label = NULL, $value = 1, $checked = NULL, $attributes = [], $labelClass = '') {

    $out = '<div class="checkbox">';
    $out .= '<label class="' . $labelClass . '">';
    $out .= Form::checkbox($name, $value, $checked ? $checked : old($name), $attributes) . $label;
    $out .= '</label>';
    $out .= '</div>';

    return $out;
});

Form::macro('inputFieldWithImagePreview', function ($nameInput, $namePreview, $srcImg = null, $label = null, $attributesInput = [], $attributesPreview = []) {
    $attributesPreview = array_merge($attributesPreview, ['id' => $namePreview]);
    $attributesInput = array_merge($attributesInput, ['accept' => 'image/*', 'data-preview-img' => '#'.$namePreview]);

    $element = '<div class="row__image"> <a class="">' . Html::image($srcImg, $namePreview, $attributesPreview) . '</a></div>' . Form::file($nameInput, field_attributes($nameInput, $attributesInput, true));
    if ($srcImg == null) {
        $element = str_replace('src="'.Request::root().'/"', 'src=""', $element);
    }
    return field_wrapper($nameInput, $label, $element);
});

Form::macro('inputFileImages', function ($nameInput, $label = null, $attributes = []) {
    $element = Form::file($nameInput, field_attributes($nameInput, $attributes));

    return field_wrapper($nameInput, $label, $element);
});

Form::macro('radioInline', function ($name, $label = NULL, array $options, $checked = NULL, $attributes = []) {
    $out = $label ? '<label for="' . $name . '">' . $label . '</label>' : '';

    $values = array_keys($options);
    $out .= '<div class="radio">';
    foreach($values as $value) {
        $isChecked = old($name, $checked) == $value;
        $out .= '<label class="form-radio form-normal form-text ';
        if($isChecked) $out .= 'active">'; else $out .= '">';
        $out .= Form::radio($name, $value, $isChecked, $attributes) . $options[$value];
        $out .= '</label>';
    }
    $out .= '</div>';
    return $out;
});

HTML::macro('flash', function () {
    $class = Session::get('flash-class') ? Session::get('flash-class') : 'success';
    $out =  '<div class="alert-wrap in animated jellyIn">';
    $out .= '<div class="alert alert-' . $class . ' alert-dismissible" id="flash"  role="alert">';
    $out .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times-circle"></i></button>';
    $out .= Session::get('flash-msg');
    $out .= '</div></div>';

    return Session::has('flash-msg') ? $out : '';

});

function field_wrapper($name, $label, $element, $icon = null)
{
    $out = '<div class="form-group';
    $out .= field_error($name) . '">';
    if (!is_null($icon)) $out = iconGroup($icon, $out, $element, $name);
    else {
        $out .= field_label($name, $label);
        $out .= $element;
        $out .= errors_msg($name);
    }
    $out .= '</div>';

    return $out;
}

function iconGroup($icon, $out, $element, $name) {
    $out .= '<div class="input-group">';
    $out .= '<div class="input-group-addon"><i class="' . $icon . '"></i></div>';
    $out .= $element;
    $out .= '</div>';
    $out .= errors_msg($name);
    return $out;
}

function field_error($field) {
    $error = '';
    if($errors = Session::get('errors')) {
        $error = $errors->first($field) ? ' has-error' : '';
    }
    return $error;
}

function errors_msg($field) {
    $errors = Session::get('errors');

    if($errors && $errors->has($field)) {
        $msg = $errors->first($field);
        return '<p class="help-block">' . $msg . '</p>';
    }

    return '';
}

function field_label($name, $label) {
    if(is_null($label)) return '';

    $name = str_replace('[]', '', $name);

    $out = '<label for="' . $name . '" class="control-label">';
    $out .= $label . '</label>';

    return $out;
}

function field_attributes($name, $attributes = [], $noClass = false)
{
    $name = str_replace('[]', '', $name);

    return array_merge(['class' => $noClass ? '' : 'form-control', 'id' => $name], $attributes);
}


function versioned_asset($path) {
    return asset($path) . '?v=' . filemtime(public_path($path));
};
