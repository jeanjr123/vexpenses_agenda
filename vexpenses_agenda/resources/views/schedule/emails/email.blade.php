@component('mail::message')
<p>Olá, {{ ucwords(strtolower($auth->name)) }}</p>
<p>O seguinte contato foi cadastrado na agenda vexpenses:</p>
<br/>

@component('mail::table')
| Nome                               | Email          | 
| -----------------------------------|:--------------:| 
|{{ucwords(strtolower($user->name))}}|{{$user->email}}| 
@endcomponent

<br/>

@endcomponent