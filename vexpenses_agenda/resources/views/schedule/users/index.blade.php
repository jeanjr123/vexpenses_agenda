@extends('layouts.app')

@section('content')
 <a class="btn btn-info mb-3" href="{{route('schedule.users.create')}}">Novo Usuario</a>
  <div class="table table-reponsive">
    <table class="table table-striped table-hover">
        <thead class="thead-dark">
            <th>Id Usuário</th>
            <th>Nome</th>
            <th>Email</th>
            <th>Categoria</th>
            <th>Telefones</th>
            <th>Endereços</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>{{$user->id}}</td>  
                <td>{{ucwords(strtolower($user->name))}}</td>  
                <td>{{$user->email}}</td>   
                <td>{{$user->category}}</td>
                <td>                            
                  <a class="btn btn-primary" href="{{route('schedule.phones.show',
                  ['phone'=> $user->id])}}" >
                       <i class="fa fa-phone"></i>
                   </a>
                </td>
                <td>
                    <a class="btn btn-primary" href="{{route('schedule.adresses.show',
                    ['adress'=> $user->id])}}" >
                        <i class="fa fa-home"></i>
                    </a>
                </td>
                <td>
                   <div class="btn-group">
                       <a class="btn btn-sm btn-warning" href="{{route('schedule.users.edit',
                       ['user'=>$user->id])}}" >
                           <i class="fa fa-pencil"></i> Editar
                       </a>
                   </div> 
                   
                   <div class="btn-group">
                    <form action="{{route('schedule.users.destroy', ['user' => $user->id])}}"
                     method="post">
                     @csrf
                     @method('delete')
                     <button type="submit" class="btn btn-sm btn-danger" >
                         <i class="fa fa-trash"></i> Excluir
                     </button>
                    </form>
                   </div>


                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
    {{$users->links()}}
   
@endsection
