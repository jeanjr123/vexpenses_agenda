@extends('layouts.app')

@section('content')
  <form action="{{route('schedule.users.update', ['user' => $user->id ] )}}"
     class="form-group" method="POST">
    @csrf
    @method('put')
      <div class="form-group row">
        <div class="col-md-6">
          <label>Nome Usuário</label>
        <input type="text" name="name" value="{{$user->name}}" class="form-control 
         @error('name') is-invalid @enderror">
            @error('name')
            <div class="invalid-feedback">
              {{$message}}
            </div>
            @enderror
      </div>
        <div class="col-md-6">
          <label>Email Usuário</label>
          <input type="email" name="email" value="{{$user->email}}" class="form-control 
          @error('email') is-invalid @enderror">
           @error('email')
             <div class="invalid-feedback">
                {{$message}}
             </div>
           @enderror
        </div>
      </div>

      <div class="form-group row">
        <div class="col-md-6">
          <label>Categoria</label>
          <select name="category_id" id="category"  class="form-control 
          @error('category_id') is-invalid  @enderror">
            <option value="">selecione uma categoria ..</option>
            @foreach ($categories as $category )
            <option value="{{$category->id}}">{{$category->category_name}}</option>
            @endforeach
          </select>
          @error('category_id')
           <div class="invalid-feedback">
             {{$message}}
           </div>
          @enderror
        </div>

        <div class="col-md-3">
            <br>
            <a class="btn btn-primary mt-2" href="{{route('schedule.phones.show',
              ['phone'=> $user->id])}}" >
                 <i class="fa fa-phone"></i> Telefones
             </a>
             <a class="btn btn-primary mt-2" href="{{route('schedule.adresses.show',
               ['adress'=> $user->id])}}" >
                 <i class="fa fa-home"></i> Endereços
             </a>
        </div>

        
      </div>


    <button class="btn btn-success" type="submit">Salvar</button>
   </form>

@endsection
