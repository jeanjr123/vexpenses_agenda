@extends('layouts.app')

@section('content')
  <form action="{{route('schedule.users.store')}}" class="form-group" method="POST">
    @csrf
      <div class="form-group row">
        <div class="col-md-6">
          <label>Nome Usuário</label>
        <input type="text" name="name" value="{{@old('name')}}" class="form-control 
         @error('name') is-invalid @enderror">
            @error('name')
            <div class="invalid-feedback">
              {{$message}}
            </div>
            @enderror
      </div>
        <div class="col-md-6">
          <label>Email Usuário</label>
          <input type="email" name="email" value="{{@old('email')}}" class="form-control 
          @error('email') is-invalid @enderror">
           @error('email')
             <div class="invalid-feedback">
                {{$message}}
             </div>
           @enderror
        </div>
      </div>

      <div class="form-group row">
        <div class="col-md-6">
          <label>Categoria</label>
          <select name="category_id" id="category"  class="form-control 
          @error('category_id') is-invalid  @enderror">
            <option value="">selecione uma categoria ..</option>
            @foreach ($categories as $category )
            <option value="{{$category->id}}">{{$category->category_name}}</option>
            @endforeach
          </select>
          @error('category_id')
           <div class="invalid-feedback">
             {{$message}}
           </div>
          @enderror
        </div>
      </div>

      <hr>
      <div class="form-group row">
        <div class="col-md-6">
          <label>Celular</label>
          <input type="text" name="mobile_phone[]" class="form-control 
          @error('mobile_phone.*') is-invalid @enderror">
          @error('mobile_phone.*')
            <div class="invalid-feedback">
              {{$message}}
            </div>
          @enderror
        </div>
        <div class="col-md-6">
          <label>Telefone</label>
          <input type="text" name="phone[]" class="form-control
          @error('phone.*') is-invalid @enderror">
          @error('phone.*')
          <div class="invalid-feedback">
            {{$message}}
          </div>
        @enderror
        </div> 
      </div>

      <!-- inserir mais de um telefone -->
      <div id="morePhones">
        <div class="morePhones">
        </div>     
      <div>

      <div class="btn-group">
        <button class="btn btn-info" id="addPhones">
         <i class="fa fa-plus"></i>
         Adicionar Telefones
        </button>    
      </div>      

      <hr>
      <div class="form-group row">

        <div class="col-md-2">
          <label>Cep</label>
          <input type="text" name="cep[]" class="form-control cep
          @error('cep.*') is-invalid @enderror">
          @error('cep.*')
          <div class="invalid-feedback">
            {{$message}}
          </div>
          @enderror
        </div> 

        <div class="col-md-4">
          <label>Rua</label>
          <input type="text" name="street[]" id="street1" class="form-control 
          @error('street.*') is-invalid @enderror">
          @error('street.*')
            <div class="invalid-feedback">
              {{$message}}
            </div>
          @enderror
       </div>

       <div class="col-md-2">
        <label>Número</label>
        <input type="text" name="number[]" class="form-control
        @error('number.*') is-invalid  @enderror">
        @error('number.*')
          <div class="invalid-feedback">
            {{$message}}
          </div>
        @enderror
       </div> 
      
        <div class="col-md-4">
          <label>Bairro</label>
          <input type="text" name="neighborhood[]" id="neighborhood1" class="form-control
          @error('neighborhood.*') is-invalid @enderror">
          @error('neighborhood.*')
            <div class="invalid-feedback">
              {{$message}}
            </div>
          @enderror
        </div>
      </div>

      <div class="form-group row">
        <div class="col-md-4">
          <label>Cidade</label>
          <input type="text" name="city[]" id="city1" class="form-control
          @error('city.*') is-invalid @enderror">
          @error('city.*')
          <div class="invalid-feedback">
            {{$message}}
          </div>
          @enderror
        </div>
        <div class="col-md-2">
          <label>Estado</label>
          <input type="text" name="state[]" id="state1" class="form-control
          @error('state.*') is-invalid @enderror">
          @error('state.*')
          <div class="invalid-feedback">
            {{$message}}
          </div>
          @enderror
        </div> 
      </div>

         <!-- inserir mais de um endereço -->
         <div id="moreAddress">
          <div class="moreAddress">
          </div>     
         <div>

      <div class="btn-group">
        <button class="btn btn-info" id="add">
         <i class="fa fa-plus"></i>
         Adicionar Telefones
        </button>    
      </div>      

      <hr>


    <button class="btn btn-success" type="submit">Cadastrar</button>
   </form>

   <script>
      numAddress = 1;

      $(document).on('blur', '.cep', function(){
         
         if(this.value){      
            $.ajax({
                url: 'http://api.postmon.com.br/v1/cep/'+this.value,
                dataType:'json',
                crossDomain: true,
                statusCode:{
                  200:function(data){
                     console.log(data);
                     $(`#street${numAddress}`).val(data.logradouro);
                     $(`#neighborhood${numAddress}`).val(data.bairro);
                     $(`#city${numAddress}`).val(data.cidade);
                     $(`#state${numAddress}`).val(data.estado);
                  },
                  400:function(msg){
                     console.log(msg);
                  },
                  404:function(msg){
                     console.log(msg); //cep invalido
                  },
                }
            })
            
          }					
      });
  
      $(document).on('click','#addPhones',function(){
          $('#morePhones .morePhones').append(
            '<div class="form-group row">\
               <div class="col-md-6">\
                 <label>Celular</label>\
                 <input type="text" name="mobile_phone[]" class="form-control">\
               </div>\
              <div class="col-md-6">\
                <label>Telefone</label>\
                <input type="text" name="phone[]" class="form-control">\
              </div>\
            </div>'
          );

          return false;
      });
    
   </script>

<script>
  
  $(document).on('click','#add',function(){
      numAddress = numAddress+1;

      $('#moreAddress .moreAddress').append(
      '<div class="form-group row">\
       <div class="col-md-2">\
       <label>Cep</label>\
       <input type="text" name="cep[]" class="cep form-control">\
       </div>\
       <div class="col-md-4">\
       <label>Rua</label>\
       <input type="text" name="street[]" id="street'+numAddress+'" class="form-control">\
       </div>\
       <div class="col-md-2">\
       <label>Número</label>\
       <input type="text" name="number[]" class="form-control">\
       </div>\
       <div class="col-md-4">\
       <label>Bairro</label>\
       <input type="text" name="neighborhood[]" id="neighborhood'+numAddress+'" class="form-control">\
       </div>\
       </div>\
       <div class="form-group row">\
       <div class="col-md-4">\
       <label>Cidade</label>\
       <input type="text" name="city[]"  id="city'+numAddress+'" class="form-control">\
       </div>\
       <div class="col-md-2">\
       <label>Estado</label>\
       <input type="text" name="state[]" id="state'+numAddress+'" class="form-control">\
       </div>\
       ');

       

      return false;
  });

</script>
@endsection
