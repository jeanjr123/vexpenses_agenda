
<div class="table-responsive" id="phoneTable">
  <table class="table table-hover" >
      <thead>
      <tr>
          <th>Celular</th>
          <th>Telefone</th>
          <th>Ações</th>  
        </tr>
        
      </thead>
      <tbody>
          @foreach ($phones as $phone)
          <tr>
            <td>{{$phone->mobile_phone}}</td>
            <td>{{$phone->phone}}</td>
            <td>
              <div class="btn-group">
                <button class="btn btn-sm btn-warning open-modal-js" 
                 data-toggle="modal" data-target="#editPhoneModal" data-target-content="#editPhones"
                 data-href="{{route('schedule.phones.edit',$phone->id)}}">
                    Editar
                </button>
              </div>

              <div class="btn-group">
                <form action="{{route('schedule.phones.destroy',['phone' => $phone->id])}}" 
                method="POST">
                  @csrf
                  @method('delete')
                  <button type="submit" class="btn btn-sm btn-danger">
                    <i class="fa fa-trash"></i>
                    Excluir
                  </button>
                </form>
              </div>
            </td>
          </tr>
          @endforeach
      </tbody>
  </table>  
</div>


<script src="{{ asset('assets/js/app.js') }}"></script>
