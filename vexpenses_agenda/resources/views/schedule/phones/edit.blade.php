<div id="editPhones">
<form action="{{route('schedule.phones.update',['phone' => $phone->id])}}" 
    class="form-group" method="POST" data-validate="true">
    @csrf
    @method('put')

      <div class="form-group row">

        <div class="col-md-6">
          <div class="form-group">
            <label>Celular</label>
            <input type="text" name="mobile_phone" id="mobile_phone" value="{{$phone->mobile_phone}}"
            class="form-control" data-rule-required="true"
            >
            @error('mobile_phone')
              <div class="invalid-feedback">
                {{$message}}
              </div>
            @enderror
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Telefone</label>
            <input type="text" name="phone" id="phone" value="{{$phone->phone}}"
            class="form-control" data-rule-required="true" >
            @error('phone')
            <div class="invalid-feedback">
              {{$message}}
            </div>
            @enderror
          </div>
        </div> 

      </div>                       
    <button class="btn btn-success"
     type="submit">Salvar</button>
   </form>

</div>
