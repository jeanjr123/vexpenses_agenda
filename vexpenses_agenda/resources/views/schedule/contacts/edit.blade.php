@extends('layouts.app')

@section('content')
  @if($contact->image != "")
  <div class="form-group row">
    <div class="col-3">
      <img src="{{asset('storage/'.$contact->image)}}" alt="" class="img-thumbnail">
      <form action="{{route('schedule.photo.remove',$contact->id)}}" method="post">
         @csrf
         <button type="submit" class="btn btn-sm btn-danger mt-1">
           Remover
         </button>
      </form>
    </div>
  </div>
  @endif
  <form action="{{route('schedule.contacts.update', ['contact' => $contact->id ] )}}"
     class="form-group" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
      <div class="form-group row">
        <div class="col-md-6">
          <label>Nome Contato</label>
        <input type="text" name="name" value="{{$contact->name}}" class="form-control 
         @error('name') is-invalid @enderror">
            @error('name')
            <div class="invalid-feedback">
              {{$message}}
            </div>
            @enderror
      </div>
        <div class="col-md-6">
          <label>Email Contato</label>
          <input type="email" name="email" value="{{$contact->email}}" class="form-control 
          @error('email') is-invalid @enderror">
           @error('email')
             <div class="invalid-feedback">
                {{$message}}
             </div>
           @enderror
        </div>
      </div>

      <div class="form-group row">
        <div class="col-md-6">
          <label>Categoria</label>
          <select name="category_id" id="category"  class="form-control 
          @error('category_id') is-invalid  @enderror">
            <option value="">selecione uma categoria ..</option>
            @foreach ($categories as $category )
            <option value="{{ $category->id }}" {{$contact->category_id == $category->id ?
             "selected" : "" }}>{{ $category->category_name }}</option>
            @endforeach
          </select>
          @error('category_id')
           <div class="invalid-feedback">
             {{$message}}
           </div>
          @enderror
        </div>
       
        <div class="col-md-6">
          <label>Foto Contato</label>
          <input type="file" name="image" value="" class="form-control">   
        </div>
      </div>
    <button class="btn btn-success" type="submit">Salvar</button>
   </form>

@endsection
