
<div class="table table-reponsive">
    <table class="table table-striped table-hover">
        <thead class="thead-dark">
            <th>Id Contato</th>
            <th>Nome</th>
            <th>Email</th>
            <th>Categoria</th>
            <tr style="page-break-after:always;"></tr>
        </thead>
        <br>
        <tbody>
            @foreach ($contacts as $contact)
            <tr>
                <td>{{$contact->id}}</td>  
                <td>{{ucwords(strtolower($contact->name))}}</td>  
                <td>{{$contact->email}}</td>   
                <td>{{$contact->category}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
