@extends('layouts.app')

@section('content')


<a class="btn btn-info float-left"
        href="{{route('schedule.contacts.create')}}">
        Novo Contato
</a>
<div class="d-flex flex-row-reverse">
       <a class="btn btn-danger ml-1 mb-1 p-2"
       href="{{route('schedule.contacts.print')}}">
        Imprimir <i class="fas fa-file-pdf"></i>
      </a>  
       <a class="btn btn-success p-2 mb-1"
          href="{{route('schedule.contacts.export')}}">
           Exportar <i class="fas fa-file-excel"></i>
       </a>  
</div>

<form action="{{route('schedule.contacts.index')}}" method="get">
<div class="form-group row">
       <div class="col-md-3 offset-6">
         {!! Form::dateField(
           'startDate','Data Inicio', null,
           [
             'data-input' => 'datetimepicker',
             'data-rule-required' => 'true',
             'class' => 'form-control'
           ]
         )!!}
      </div>
      <div class="col-md-3">
       {!! Form::dateField(
         'endDate','Data Fim', null,
         [
           'data-input' => 'datetimepicker',
           'data-rule-required' => 'true',
           'class' => 'form-control'
         ]
       )!!}
    </div>
    
</div>
<div class="btn group offset-11">
       <button class="btn btn-success">Buscar</button>
</div>
</form>
 
@include('schedule.contacts.index_table')

@include('schedule.phones.modals.show_phone')

@include('schedule.phones.modals.edit_phone')

@include('schedule.contacts.modals.photo_contact')

@include('schedule.adresses.modals.show_address')

@include('schedule.adresses.modals.edit_address')


   
@endsection
