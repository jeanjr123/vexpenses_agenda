<div class="form-group row">
    <div class="col-md-6">
     {!! Form::textField('name', 'Nome Usuário', null,
     [
      'data-rule-required' => 'true',
       'class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : null)
     ]) !!}
     @error('name')
      <div class="invalid-feedback">
        {{$message}}
      </div>
     @enderror
  </div>
    <div class="col-md-6">
      {!! Form::textField('email', 'Email Usuário', null,
      [
       'data-rule-required' => 'true',
       'type' => 'email',
       'class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : null)
      ]) !!}
       @error('email')
         <div class="invalid-feedback">
            {{$message}}
         </div>
       @enderror
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-6">
      {!! Form::selectField('category_id', 'Categoria',
        $categories,
        null,
        [
          'data-input' => 'selectize',
          'class' => 'form-control'. ($errors->has('category_name') ? ' is-invalid' : null)
        ]) !!}
        @error('category_name')
          <div class="invalid-feedback">
            {{$message}}
          </div>
        @enderror
    </div>

    <div class="col-md-6">
      {!! Form::inputFileImages('image','Foto Usuário',
       [
         'class' => 'form-control',
         'type'  => 'file'
       ]) !!}
    </div>
  </div>

  <hr class="border border-secondary">
  <h4>Telefones do contato</h4>
  <div class="form-group row">
    <div class="col-md-6">
      {!! Form::textField(
        'mobile_phone[]','Celular', null,
        [
          'data-rule-required' => 'true',
          'data-rule-fone' => 'true',
          'class' => 'form-control mask mask__fone'
        ]
      )!!}
    </div>
    <div class="col-md-6">
      {!! Form::textField(
        'phone[]','Telefone', null,
        [
          'data-rule-required' => 'true',
          'data-rule-fone' => 'true',
          'class' => 'form-control mask mask__fone',
         
        ]
      )!!}
    </div> 
  </div>

  <!-- inserir mais de um telefone -->
  <div id="morePhones">
    <div class="morePhones">
    </div>     
  <div>

 <div id="btnsPhone">
   <div class="btn-group">
     <button class="btn btn-info" id="addPhones" value="1">
      <i class="fa fa-plus"></i>
      Adicionar Telefone
     </button>  
   </div>  
 </div>

  <hr class="border border-secondary">
  <h4>Endereços do contato</h4>
  <div class="form-group row">

    <div class="col-md-2">
      {!! Form::textField(
        'cep[]','Cep', null,
        [
          'id' => '1',
          'data-rule-required' => 'true',
          'class' => 'form-control cep'
        ]
      )!!}
    </div> 

    <div class="col-md-4">
      {!! Form::textField(
        'street[]','Rua', null,
        [
          'id' => 'street1',
          'data-rule-required' => 'true',
          'class' => 'form-control'
        ]
      )!!}
   </div>

   <div class="col-md-2">
      {!! Form::textField(
        'number[]','Número', null,
        [
          'data-rule-required' => 'true',
          'class' => 'form-control'
        ]
      )!!}
   </div> 
  
    <div class="col-md-4">
      {!! Form::textField(
        'neighborhood[]','Bairro', null,
        [
          'id' => 'neighborhood1',
          'data-rule-required' => 'true',
          'class' => 'form-control'
        ]
      )!!}
    </div>
  </div>

  <div class="form-group row">
    <div class="col-md-4">
      {!! Form::textField(
        'city[]','Cidade', null,
        [
          'id' => 'city1',
          'data-rule-required' => 'true',
          'class' => 'form-control'
        ]
      )!!}
    </div>
    <div class="col-md-2">
      {!! Form::textField(
        'state[]','Estado', null,
        [
          'id' => 'state1',
          'data-rule-required' => 'true',
          'class' => 'form-control'
        ]
      )!!}
    </div> 
  </div>

     <!-- inserir mais de um endereço -->
     <div id="moreAddress">
      <div class="moreAddress">
      </div>     
     <div>

  <div id="btnsAddress">
    <div class="btn-group" >
      <button class="btn btn-info" id="add" value="1">
       <i class="fa fa-plus"></i>
       Adicionar Endereços
      </button>    
    </div>   
  </div>
     

  <hr class="border border-secondary">


<button class="btn btn-success" type="submit">Cadastrar</button>