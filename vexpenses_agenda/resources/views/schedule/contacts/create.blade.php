@extends('layouts.app')

@section('content')
<h4>Dados pessoais do contato</h4>
  {{ Form::open(['route' => 'schedule.contacts.store', 'data-validate', 
   'files' => true, 'method' => "POST", 'class="form-group"']) }}
    @csrf
    @include('schedule.contacts.fields')
  {!! Form::close() !!}
@endsection
