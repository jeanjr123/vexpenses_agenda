<div class="table table-reponsive">
    <table class="table table-striped table-hover data-table" data-page-length='5' >
        <thead class="thead-dark">
            <th>Id Contato</th>
            <th>Nome</th>
            <th>Email</th>
            <th>Categoria</th>
            <th>Foto</th>
            <th>Telefones</th>
            <th>Endereços</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach ($contacts as $contact)
            <tr>
                <td>{{$contact->id}}</td>  
                <td>{{ucwords(strtolower($contact->name))}}</td>  
                <td>{{$contact->email}}</td>   
                <td>{{$contact->category}}</td>
                <th>
                    <button class="btn btn-primary open-modal-photo-js" data-toggle="modal" 
                    data-target="#photoModal" data-target-content="#showContactPhoto"
                    data-target-attr="src"
                    data-href="{{route('schedule.contacts.show', $contact->id)}}"
                    >
                        <i class="fa fa-user"></i>
                    </button>
                </th>
                <td>                            
                  <button class="btn btn-primary open-modal-js" 
                  data-toggle="modal" data-target="#phoneModal" data-target-content="#phoneTable"
                  data-href="{{route('schedule.phones.show',$contact->id)}}">
                       <i class="fa fa-phone"></i>
                   </button>
                </td>
                <td>
                    <button class="btn btn-primary open-modal-js" 
                    data-toggle="modal" data-target="#addressModal" data-target-content="#addressTable"
                    data-href="{{route('schedule.adresses.show', $contact->id)}}">
                        <i class="fa fa-home"></i>
                    </button>
                </td>
                <td>
                   <div class="btn-group">
                       <a class="btn btn-sm btn-warning" href="{{route('schedule.contacts.edit',
                       ['contact'=>$contact->id])}}" >
                           <i class="fa fa-pencil"></i> Editar
                       </a>
                   </div> 

                   <div class="btn-group">
                    <form action="{{route('schedule.contacts.destroy', $contact->id)}}"
                     method="post" >
                     @csrf
                     @method('delete')
                     <button type="submit" class="btn btn-sm btn-danger" >
                         <i class="fa fa-trash"></i> Excluir
                     </button>
                    </form>
                   </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
