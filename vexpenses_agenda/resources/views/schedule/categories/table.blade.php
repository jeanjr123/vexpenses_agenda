<table class="table table-striped table-hover data-table" data-page-length='5'>
    <thead class="thead-dark">
        <th>Id Categoria</th>
        <th>Nome Categoria</th>
        <th>Ações</th>
    </thead>
    <tbody>
        @foreach ($categories as $category)
        <tr>
            <td>{{$category->id}}</td>  
            <td>{{$category->category_name}}</td>  
            <td>
              <div class="btn-group">
                <a class="btn btn-sm btn-warning"
                href="{{route('schedule.categories.edit', 
                ['category' => $category->id])}}">Editar</a>    
              </div>

              <div class="btn-group">
                  {!! Form::open([
                     'route' => ['schedule.categories.destroy', $category->id],
                     'method' => 'DELETE'
                  ]) !!}
                 
                    <button class="btn btn-sm btn-danger" type="submit">
                      <i class="fa fa-trash"></i>
                      Excluir
                    </button>
                  {!! Form::close() !!}
              </div>
            </td>  
        </tr>
        @endforeach
    </tbody>
</table>
