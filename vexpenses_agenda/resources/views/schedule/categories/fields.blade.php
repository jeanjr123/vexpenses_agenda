
<div class="form-group">
    {!! Form::label('Nome Categoria') !!}
    {!! Form::text('category_name', null,
    ['class' => 'form-control' . ($errors->has('category_name') ? ' is-invalid' : null)]) !!}
    @error('category_name')
          <div class="invalid-feedback">
             {{$message}}
          </div>
    @enderror
</div>
