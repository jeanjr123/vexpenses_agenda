@extends('layouts.app')

@section('content')
  <h2>Editar Categoria</h2>
<form action="{{route('schedule.categories.update',
 ['category' => $category->id])}}" class="form-group" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nome Categoria</label>
        <input type="text" name="category_name" value="{{$category->category_name}}" 
        class="form-control">
    </div>
    <button class="btn btn-success" type="submit">Salvar</button>
</form>
@endsection
