@extends('layouts.app')

@section('content')
  <form action="{{route('schedule.categories.store')}}" class="form-group" method="POST" data-validate>
    @csrf
    @include('schedule.categories.fields')
    <button class="btn btn-success" type="submit">Cadastrar</button>
  </form>
@endsection
