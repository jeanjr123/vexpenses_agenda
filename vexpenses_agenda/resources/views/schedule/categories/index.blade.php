@extends('layouts.app')

@section('content')
<a class="btn btn-info mb-2" href="{{route('schedule.categories.create')}}" >Nova Categoria</a>
    @include('schedule.categories.table')
@endsection
