
<div class="table table-responsive">
    <table class="table table-hover">
        <thead>
            <th>Id Endereço</th>
            <th>Rua</th>
            <th>Numero</th>
            <th>Bairro</th>
            <th>Cep</th>
            <th>Cidade</th>
            <th>Estado</th>
            <th>Ações</th>
        </thead>
        <tbody>
            @foreach ($adresses as $address)
            <tr>
              <td>{{$address->id}}</td>
              <td>{{ucwords(strtolower($address->street))}}</td>
              <td>{{$address->number}}</td>
              <td>{{ucwords(strtolower($address->neighborhood))}}</td>
              <td>{{$address->cep}}</td>
              <td>{{ucwords(strtolower($address->city))}}</td>
              <td>{{ucwords(strtolower($address->state))}}</td>
              <td>
                <div class="btn-group">
                    <button class="open-modal-js btn btn-sm btn-warning" data-toggle="modal" 
                    data-target="#editAddressModal" data-target-content="#editAddress"
                    data-href="{{route('schedule.adresses.edit',$address->id)}}">
                    Editar
                    </button>    
                </div>

                <div class="btn-group mt-1">
                     <form action="{{route('schedule.adresses.destroy', ['adress' => $address->id])}}"
                      method="POST">
                        @csrf  
                        @method('delete')
                        <button class="btn btn-sm btn-danger" type="submit">
                          Excluir
                        </button>
                      </form>
                </div>
              </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$adresses->links()}}
</div>
<script src="{{ asset('assets/js/app.js') }}"></script>
