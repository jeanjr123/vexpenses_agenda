
<form action="{{route('schedule.adresses.update', ['adress' => $address->id])}}"
     class="form-group" method="POST" data-validate="true">
    @csrf
    @method('put')
      <div class="form-group row">

        <div class="col-md-2">
          <div class="form-group">
            <label>Cep</label>
            <input type="text" name="cep" id="1"  value="{{$address->cep}}" 
            class="cep form-control" data-rule-required="true">
            @error('cep')
            <div class="invalid-feedback">
              {{$message}}
            </div>
            @enderror
          </div>
        </div> 

        <div class="col-md-4">
         <div class="form-group">
          <label>Rua</label>
          <input type="text" name="street" id="street1" value="{{$address->street}}"
           class="form-control" data-rule-required="true">       
             @error('street')
             <div class="invalid-feedback">
               {{$message}}
              </div>
             @enderror
         </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Número</label>
            <input type="text" name="number" value="{{$address->number}}"
            class="form-control" data-rule-required="true">
            @error('number')
            <div class="invalid-feedback">
              {{$message}}
              </div>
            @enderror
          </div>
        </div> 
  
        <div class="col-md-4">
          <div class="form-group">
            <label>Bairro</label>
            <input type="text" name="neighborhood" id="neighborhood1"
            value="{{$address->neighborhood}}" 
            class="form-control" data-rule-required="true">
            @error('neighborhood')
            <div class="invalid-feedback">
              {{$message}}
            </div>
            @enderror
          </div>
        </div>

      </div>

      <div class="form-group row">

        <div class="col-md-4">
          <div class="form-group">
            <label>Cidade</label>
            <input type="text" name="city" id="city1" value="{{$address->city}}" 
            class="form-control" data-rule-required="true"> 
            @error('city')
            <div class="invalid-feedback">
              {{$message}}
            </div>
            @enderror
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Estado</label>
            <input type="text" name="state" id="state1" value="{{$address->state}}" 
            class="form-control" data-rule-required="true">
            @error('state')
            <div class="invalid-feedback">
              {{$message}}
            </div>
            @enderror
          </div>
        </div> 

      </div>

    <button class="btn btn-success" type="submit">Salvar</button>
   </form>
   
