
const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const babelify = require('babelify');
const stringify = require('stringify');
const { argv } = require('yargs');
const aliasify = require('aliasify');
const eslintify = require('eslintify');
const LessPluginAutoPrefix = require('less-plugin-autoprefix');
const aliases = require('./aliases');

//cria prefix constante tenta converter todo lass para func a ultm 2 versoes de navegador
const autoprefix = new LessPluginAutoPrefix({
  browsers: ['last 2 versions'],
});

//onde pega o js inscrito e pra onde envia compilado
const lessPath = 'resources/assets/less'; //origem
const jsPath = 'resources/assets/js/src';
const cssDest = 'public/assets/css';
const jsDest = 'public/assets/js';


gulp.task('browserify', (done) => {
  let fileName = '/app.js';
  
  //argumento --admin administrador exemplo
//   if (argv.app) {
//     fileName = '/app.js';
//   } else if (argv.admin) {
//     fileName = '/admin.js';
//   } else {
//     console.log('Rotina não encontrada!');
//     return;
//   }

  gulp
    .src(jsPath + fileName, {
      read: false,
    })             //pacote deixa rotas do php deixa acessivel ao js
    .pipe($.shell('php artisan laroute:generate'))
    .pipe(
      $.bro({ //adaptação para js todos navegadores ultimas 2 versoes
        transform: [
          [eslintify, { 'quiet-ignored': true }],
          babelify,
          stringify(['.html']),
          [aliasify, aliases],
        ],
        error: $.notify.onError('Error: <%= error.message %>'),
      }),
    )
    .pipe(
      $.plumber({
        errorHandler: $.notify.onError('Error: <%= error.message %>'),
      }),
    )
    .pipe(gulp.dest(jsDest)) //ende vai mandar o arquivo
    .pipe(
      $.rename({
        suffix: '.min',
      }),
    )
    .pipe(
      $.sourcemaps.init({
        identityMap: true,
        loadMaps: true,
        debug: true,
      }),
    )
    .pipe($.uglify())    //minifica
    .pipe($.sourcemaps.write('./')) 
    .pipe(gulp.dest(jsDest))
    .pipe($.notify('Build de javascript finalizada'));
  done();
});

//pega o ES6 e compila para um js que os navegadores entendam
gulp.task('lint', (done) => {
  return gulp
    .src(['gulpfile.js', 'resources/assets/js/src/*/.js'])
    .pipe($.eslint())
    .pipe($.eslint.format());

  done();
});

gulp.task('css', (done) => {
  let src = lessPath + '/app/app.less';

  gulp
    .src(src)
    .pipe(
      $.plumber({
        errorHandler: $.notify.onError('Error: <%= error.message %>'),
      }),
    )
    .pipe(
      $.less({
        plugins: [autoprefix],
        javascriptEnabled: true,
      }),
    )
    .pipe(gulp.dest(cssDest))
    .pipe(
      $.rename({
        suffix: '.min',
      }),
    )
    .pipe($.csso())
    .pipe(gulp.dest(cssDest))
    .pipe($.notify('Build de CSS finalizada'));

  done();
});


//task pega os arquivos e compila e gera um so libs.js
gulp.task('libs', (done) => {
  gulp
    .src([
      'node_modules/jquery/dist/jquery.js',
      'node_modules/popper.js/dist/umd/popper.min.js',
      'node_modules/bootstrap/dist/js/bootstrap.js',
      'node_modules/jquery-validation/dist/jquery.validate.js',
      'node_modules/jquery-validation/dist/localization/messages_pt_BR.js',
      'node_modules/datatables/media/js/jquery.dataTables.min.js',
      'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
      'node_modules/selectize/dist/js/standalone/selectize.js',
      'node_modules/jquery-mask-plugin/dist/jquery.mask.js',
      
    ])
    .pipe($.concat('libs.js'))
    .pipe(gulp.dest(jsDest))
    .pipe($.rename('libs.min.js'))
    .pipe($.uglify())
    .pipe(gulp.dest(jsDest))
    .pipe($.notify('Libs finalizado!'));

  done();
});

//gulp watch assiste os arquivos e recompila
gulp.task(
  'watch',
  gulp.series('lint', 'browserify', 'css', (done) => {
    gulp.watch(`${jsPath}/**/*.js`, gulp.series('lint'));
    gulp.watch(`${jsPath}/**/*.js`, gulp.series('browserify'));
    gulp.watch(`${lessPath}/**/*.less`, gulp.series('css'));
done();

  }),
);

//copia fonts do bootstrap
gulp.task('copy', (done) => {
  gulp.src(['node_modules/bootstrap/fonts/', 'node_modules/font-awesome/fonts/']).pipe(
    $.copy('public/assets/fonts', {
      prefix: 3,
    }),
  );
  done();
});
