<?php

namespace App\Models;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $table = 'phone_number';

    protected $fillable = [
        'phone', 'mobile_phone',
    ];

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }
}
