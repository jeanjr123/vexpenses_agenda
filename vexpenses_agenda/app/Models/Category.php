<?php

namespace App\Models;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
         'category_name'
    ];

    protected $table = 'category';

    public function contact()
    {
        return $this->hasMany(Contact::class);
    }
}
