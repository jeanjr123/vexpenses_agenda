<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
       'name', 'email', 'user_id', 'category_id', 'image',
    ];

    public function address()
    {
        return $this->hasMany(Address::class);
    }

    public function phone()
    {
        return $this->hasMany(Phone::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_user');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
