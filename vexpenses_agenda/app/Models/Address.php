<?php

namespace App\Models;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
     protected $table = 'address';

     protected $fillable = [
         'street','number','cep','neighborhood','city','state',

     ];


     public function contact()
     {
         return $this->belongsTo(Contact::class);
     }
}
