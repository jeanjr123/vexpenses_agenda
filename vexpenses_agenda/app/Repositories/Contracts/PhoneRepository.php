<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PhoneRepository.
 *
 * @package namespace App\Repositories;
 */
interface PhoneRepository extends RepositoryInterface
{
}
