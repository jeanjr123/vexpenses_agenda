<?php

namespace App\Exports;

use App\Models\Category;
use App\Models\Contact;
use App\Models\Phone;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ContactsExport implements FromCollection, WithHeadings, ShouldAutoSize
{


    public function headings(): array
    {
        return [
            'Cód Contato',
            'Nome Contato',
            'E-mail Contato',
            'Cód Categoria',
            'Categoria',
            'Telefone',
            'Celular'
        ];
    }

    


    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $contacts = Contact::where('user_id', auth()->user()->id)
        ->get(['id','name','email','category_id']);

        foreach ($contacts as $contact) {
            $phones = Phone::where('contact_id', $contact->id)->first();

            $category = Category::find($contact->category_id);

            $contact->category = $category->category_name;

            $contact->phone = $phones->phone;

            $contact->mobile_phone = $phones->mobile_phone;
        }

        return $contacts;
    }
}
