<?php

namespace App\Services;

use App\Repositories\Contracts\AddressRepository;

class AddressService
{
    public function __construct(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }


    public function findAddress($idAddress)
    {
        $address = $this->addressRepository->find($idAddress);

        return $address;
    }
         
    
    public function findAddressByContact($contactId)
    {
        $adresses = $this->addressRepository->where('contact_id', $contactId)->paginate(5);

        return $adresses;
    }


    public function createAddress($data, $contact)
    {
        $sizeArrAddress = count($data['street']);

        for ($i = 0; $i < $sizeArrAddress; $i++) {
            $contact->address()->create([
                'street' => $data['street'][$i],
                'number' => $data['number'][$i],
                'neighborhood' => $data['neighborhood'][$i],
                'cep' => $data['cep'][$i],
                'city' => $data['city'][$i],
                'state' => $data['state'][$i]
            ]);
        }
    }


    public function updateAddress($data, $idAddress)
    {
        $address = $this->addressRepository->update($data, $idAddress);

        return $address;
    }


    public function deleteAddress($idAddress)
    {
        $address = $this->addressRepository->find($idAddress);

        $this->addressRepository->delete($idAddress);

        return $address->contact_id;
    }

    public function deleteAddressWhere($contactId)
    {
        $this->addressRepository->deleteWhere(['contact_id' => $contactId]);
    }
}
