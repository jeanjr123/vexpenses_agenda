<?php

namespace App\Services;

use App\Jobs\SendEmailJob;
use App\Models\User;
use App\Repositories\Contracts\CategoryRepository;
use Illuminate\Support\Facades\Auth;
use stdClass;

class CategoryService
{
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }



    public function findAllCategories()
    {
        $categories = $this->categoryRepository->all();

        return $categories;
    }

    public function findAllCategoriesCombobox()
    {
        $categories = $this->categoryRepository->all()->pluck('category_name', 'id');

        return $categories;
    }


    public function findCategory($idCategory)
    {
        $category = $this->categoryRepository->find($idCategory);

        return $category;
    }

    public function findCategoryWhere($column, $value)
    {
        $category = $this->categoryRepository->where($column, $value);

        return $category;
    }


    public function createCategory($data)
    {
        $this->categoryRepository->create($data);
    }

    public function createContactByCategory($data)
    {
        $category = $this->findCategory($data['category_id']);

        $contact =  $category->contact()->create($data);

        $auth = new stdClass();

        $auth->name  = Auth::user()->name;

        $auth->email = Auth::user()->email;

        SendEmailJob::dispatch($contact, $auth)->delay(now()->addSeconds('15'));

        return $contact;
    }


    public function updateCategory($data, $idCategory)
    {
        $this->categoryRepository->update($data, $idCategory);
    }

    public function deleteCategory($idCategory)
    {
        $this->categoryRepository->delete($idCategory);
    }
}
