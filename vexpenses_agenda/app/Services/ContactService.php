<?php

namespace App\Services;

use App\Repositories\Contracts\ContactRepository;
use App\Services\CategoryService;

class ContactService
{
    public function __construct(
        CategoryService $categoryService,
        ContactRepository $contactRepository
    ) {
        $this->contactRepository = $contactRepository;

        $this->categoryService = $categoryService;
    }


    public function findAllContacts($userId)
    {
        $contacts = $this->contactRepository->where('user_id', $userId)->get();

        foreach ($contacts as $contact) {
            $category = $this->categoryService->findCategory($contact->category_id);

            $contact->category = $category->category_name;
        }

        return $contacts;
    }


    public function findContact($idContact)
    {
        $contact = $this->contactRepository->find($idContact);

        return  $contact;
    }

    public function findContactWhere($column, $value)
    {
        $contact = $this->contactRepository->where($column, $value)->get();

        return $contact;
    }

    public function findContactBetwen($field, $where)
    {
        $user_id = auth()->user()->id;

        $contacts = $this->contactRepository->findWhereBetween($field, $where)
        ->where('user_id', $user_id);

        return $contacts;
    }


    public function updateContact($idContact, $data)
    {
        $this->contactRepository->update($data, $idContact);
    }


    public function deleteContact($contactId)
    {
        $this->contactRepository->delete($contactId);
    }

    public function deleteContactPhoto($contactId)
    {
        $this->contactRepository->update(['image' => ''], $contactId);
    }
}
