<?php

namespace App\Services;

use App\Repositories\Contracts\PhoneRepository;

class PhoneService
{
    public function __construct(PhoneRepository $phoneRepository)
    {
        $this->phoneRepository = $phoneRepository;
    }


    public function findPhoneByContact($contactId)
    {
        $phone = $this->phoneRepository->where('contact_id', $contactId)->paginate(5);
        return $phone;
    }
 

    public function findPhone($idPhone)
    {
        $phone = $this->phoneRepository->find($idPhone);

        return $phone;
    }

    public function createPhone($data, $contact)
    {
        $sizeArrPhone = count($data['phone']);
  
        for ($i = 0; $i < $sizeArrPhone; $i++) {
            $contact->phone()->create([
                'phone' => $data['phone'][$i],
                'mobile_phone' => $data['mobile_phone'][$i]
            ]);
        }
    }


    public function updatePhone($data, $idPhone)
    {
        $phone = $this->phoneRepository->update($data, $idPhone);

        return $phone->contact_id;
    }


    public function deletePhone($idPhone)
    {
        $phone = $this->phoneRepository->find($idPhone);

        $this->phoneRepository->delete($idPhone);
       
        return $phone->contact_id;
    }

    public function deletePhoneWhere($contactId)
    {
        $this->phoneRepository->deleteWhere(['contact_id' => $contactId]);
    }
}
