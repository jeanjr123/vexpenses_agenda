<?php

namespace App\Http\Middleware;

use App\Services\CategoryService;
use Closure;

class CheckCategoryName
{

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $categoryName = $request->category_name;

        $category = $this->categoryService->
        findCategoryWhere('category_name', $categoryName)->get();

        if (count($category) > 0) {
            if (strtolower($category[0]->category_name) == strtolower($categoryName)) {
                flash('Categoria já cadastrada, não sendo possivel novo cadastro!')->error();

                return redirect()->route('schedule.categories.create');
            }
        }
        return $next($request);
    }
}
