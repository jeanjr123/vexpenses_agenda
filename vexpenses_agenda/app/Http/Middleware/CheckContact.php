<?php

namespace App\Http\Middleware;

use Closure;

use App\Services\ContactService;

class CheckContact
{
    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $contact = $this->contactService->findContactWhere('category_id', $request->category);
       
        if (count($contact) > 0) {
            flash('Categoria possui contato vinculado, não podendo ser excluída!')->error();

            return redirect()->route('schedule.categories.index');
        }

        return $next($request);
    }
}
