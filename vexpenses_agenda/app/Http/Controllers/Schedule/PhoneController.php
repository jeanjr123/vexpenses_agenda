<?php

namespace App\Http\Controllers\Schedule;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestPhoneEdit;
use Illuminate\Http\Request;
use App\Services\PhoneService;
use App\Services\ContactService;

class PhoneController extends Controller
{
    protected $phoneService;

    protected $contactService;

    /**
     * Construtor
     */
    public function __construct(PhoneService $phoneService, ContactService $contactService)
    {
        $this->phoneService = $phoneService;

        $this->contactService = $contactService;
    }



    /**
     * Retorna a visualização dos telefones de um usuário
     *
     * @param int $userId
     *
     * @return View
     */
    public function show($contactId)
    {
        $phones = $this->phoneService->findPhoneByContact($contactId);

        $contact = $this->contactService->findContact($contactId);

        return view('schedule.phones.index', compact('phones', 'contact'));
    }

     /**
     * Retorna a visualização do formulário para edição de determinado telefone
     *
     * @param int $phoneId
     *
     * @return void
     */
    public function edit($phoneId)
    {
        $phone = $this->phoneService->findPhone($phoneId);

        return view('schedule.phones.edit', compact('phone'));
    }

    /**
     * Atualiza um determinado telefone no banco
     *
     * @param int $idPhone
     *
     * @param Request $request
     *
     * @return View
     */
    public function update(RequestPhoneEdit $request, $idPhone)
    {
        $data = $request->all();
          
        $this->phoneService->updatePhone($data, $idPhone);

        flash('Telefone Atualizado com Sucesso!')->success();

       // return redirect()->route('schedule.phones.show', ['phone' => $contactId]);

        return redirect()->route('schedule.contacts.index');
    }

    /**
     * Exclui um determinado telefone no banco
     *
     * @param int $phoneId
     *
     * @return View
     */
    public function destroy($phoneId)
    {
        $this->phoneService->deletePhone($phoneId);

        flash('Telefone Excluido com Sucesso')->success();

        return redirect()->route('schedule.contacts.index');
    }
}
