<?php

namespace App\Http\Controllers\Schedule;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestCategory;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Middleware\CheckContact;

class CategoryController extends Controller
{
     private $categoryService;
    
    /**
    * Construtor
    */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;

        $this->middleware('checkContact', ['only' => ['destroy']]);

        $this->middleware('checkCategoryName', ['only' => ['store','update']]);
    }

    /**
     * Retorna a visualização de todas as categorias existentes no banco
     *
     * @param
     *
     * @return View
     */
    public function index()
    {
        $categories = $this->categoryService->findAllCategories();

        return view('schedule.categories.index', compact('categories'));
    }

    /**
     * Retorna o formulário de criação das categorias
     *
     * @param
     *
     * @return View
     */
    public function create()
    {
        return view('schedule.categories.create');
    }

    /**
     * Salva a categoria no banco
     *
     * @param Request $request
     *
     * @return View
     */
    public function store(RequestCategory $request)
    {
         
          $data = $request->all();

          $this->categoryService->createCategory($data);

          flash('Categoria cadastrada com sucesso!')->success();

          return redirect()->route('schedule.categories.index');
    }


   
     /**
     * Retorna a visualização do formulário para edição de determinada categoria
     *
     * @param int $category
     *
     * @return void
     */
    public function edit($category)
    {
        $category = $this->categoryService->findCategory($category);

        return view('schedule.categories.edit', compact('category'));
    }

    /**
     * Atualiza determinada categoria no banco
     *
     * @param Request $request
     *
     * @param int $id
     *
     * @return View
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $this->categoryService->updateCategory($data, $id);

        flash('Categoria Atualizada com sucesso!')->success();

        return redirect()->route('schedule.categories.index');
    }

   /**
     * Exclui determinada categoria do banco de dados
     *
     * @param int $id
     *
     * @return View
     */
    public function destroy($id)
    {
        $this->categoryService->deleteCategory($id);

        flash('Categoria Excluida com Sucesso!')->success();

        return redirect()->route('schedule.categories.index');
    }
}
