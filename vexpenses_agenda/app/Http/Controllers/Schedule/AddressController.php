<?php

namespace App\Http\Controllers\Schedule;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestAddressEdit;
use App\Services\AddressService;
use App\Services\ContactService;
use Illuminate\Http\Request;

class AddressController extends Controller
{

    protected $addressService;

    protected $userService;

    protected $contactService;
   
     /**
     * Construtor
     */
    public function __construct(AddressService $addressService, ContactService $contactService)
    {
         $this->addressService = $addressService;

         $this->contactService = $contactService;
    }
  

    /**
     * Retorna a visualização dos endereços de um usuário
     *
     * @param int $userId
     *
     * @return View
     */
    public function show($contactId)
    {
        $adresses = $this->addressService->findAddressByContact($contactId);

        $contact = $this->contactService->findContact($contactId);

        return view('schedule.adresses.index', compact('adresses', 'contact'));
    }

     /**
     * Retorna a visualização do formulário para edição de determinado endereço
     *
     * @param int $addressId
     *
     * @return void
     */
    public function edit($addressId)
    {
        $address = $this->addressService->findAddress($addressId);

        return view('schedule.adresses.edit', compact('address'));
    }

   /**
     * Atualiza um determinado endereço no banco
     *
     * @param int $addressId
     *
     * @param Request $request
     *
     * @return View
     */
    public function update(RequestAddressEdit $request, $addressId)
    {
        $data = $request->all();

        $this->addressService->updateAddress($data, $addressId);

        flash('Endereço Atualizado com Sucesso!')->success();

        return redirect()->route('schedule.contacts.index');
    }

    /**
     * Exclui um determinado endereço no banco
     *
     * @param int $addressId
     *
     * @return View
     */
    public function destroy($addressId)
    {
        $this->addressService->deleteAddress($addressId);

        flash('Endereço Excluído com Sucesso!')->success();

        return redirect()->route('schedule.contacts.index');
    }
}
