<?php

namespace App\Http\Controllers\Schedule;

use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
use App\Services\PhoneService;
use App\Exports\ContactsExport;
use App\Services\AddressService;
use App\Services\ContactService;
use App\Services\CategoryService;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\RequestContact;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\RequestContactEdit;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon as SupportCarbon;

class ContactController extends Controller
{
    private $addressService;

    private $categoryService;

    private $contactService;

    private $phoneService;

    public function __construct(
        AddressService $addressService,
        CategoryService $categoryService,
        PhoneService $phoneService,
        ContactService $contactService
    ) {
        $this->addressService = $addressService;

        $this->categoryService = $categoryService;

        $this->phoneService = $phoneService;

        $this->contactService = $contactService;
    }

    /**
     * Retorna a visualização de todos os contatos existentes no banco
     * os quais foram cadastrados pelo usuário autenticado
     *
     * @param
     *
     * @return View
     */
    public function index(Request $request)
    {
       
        $userAuthId = auth()->user()->id;

        if ($request->all()) {
            $startDate = DateTime::createFromFormat('d/m/Y H:i:s', $request->startDate);

            $startDate->format('Y/m/d H:i:s');
            
            $endDate = DateTime::createFromFormat('d/m/Y H:i:s', $request->endDate);

            $endDate->format('Y/m/d H:i:s');

            $contacts = $this->contactService
            ->findContactBetwen('created_at', [$startDate, $endDate]);
        } elseif (!$request->all()) {
            $contacts = $this->contactService->findAllContacts($userAuthId);
        }

        return view('schedule.contacts.index', compact('contacts'));
    }

    /**
     * Retorna o formulário de criação dos contatos
     *
     * @param
     *
     * @return View
     */
    public function create()
    {
        $categories = $this->categoryService->findAllCategoriesCombobox();

        return view('schedule.contacts.create', compact('categories'));
    }


    /**
     * Salva o contato no banco
     *
     * @param RequestContact $request
     *
     * @return View
     */
    public function store(RequestContact $request)
    {
        $data = $request->all();

        $data['user_id'] = auth()->user()->id;

        $request->hasFile('image') ? $data = $this->uploadImage($data, $request) : null;

        $contact = $this->categoryService->createContactByCategory($data);

        $this->addressService->createAddress($data, $contact);

        $this->phoneService->createPhone($data, $contact);

        flash('Contato salvo com sucesso!')->success();

        return redirect()->route('schedule.contacts.index');
    }


    /**
     * Retorna a visualização da foto de um usuário
     *
     * @param int $idContact
     *
     * @return string
     */
    public function show($idContact)
    {
        $contact = $this->contactService->findContact($idContact);

        return $contact->image;
    }


    /**
     * Retorna a visualização do formulário para edição de determinado contato
     *
     * @param int $idContact
     *
     * @return void
     */
    public function edit($idContact)
    {
        $contact = $this->contactService->findContact($idContact);

        $categories = $this->categoryService->findAllCategories();

        return view('schedule.contacts.edit', compact('contact', 'categories'));
    }


    /**
     * Atualiza determinado contato no banco
     *
     * @param RequestContactEdit $request
     *
     * @param int $idContact
     *
     * @return View
     */
    public function update(RequestContactEdit $request, $idContact)
    {
        $data = $request->all();

        $request->hasFile('image') ? $data = $this->uploadImage($data, $request) : null;

        $this->contactService->updateContact($idContact, $data);

        flash('Contato Atualizado com Sucesso!')->success();

        return redirect()->route('schedule.contacts.index');
    }

    /**
     * Exclui determinado contato do banco de dados
     *
     * @param int $contactId
     *
     * @return View
     */
    public function destroy($contactId)
    {
        $this->phoneService->deletePhoneWhere($contactId);

        $this->addressService->deleteAddressWhere($contactId);

        $this->contactService->deleteContact($contactId);

        flash('Contato Excluído com Sucesso!')->success();

        return redirect()->route('schedule.contacts.index');
    }

    /**
     * Grava foto do contato no banco de dados
     *
     * @param void $data
     *
     * @param object $request
     *
     * @return void
     */
    private function uploadImage($data, $request)
    {
        $image = $request->file('image');

        $data['image'] = $image->store('products', 'public');

        return $data;
    }


    /**
     * Exclui foto do contato no banco de dados e storage
     *
     * @param int $contactId
     *
     * @return View
     */
    public function removePhoto($contactId)
    {
        $contact = $this->contactService->findContact($contactId);

        $photoName = $contact->image;

        if (Storage::disk('public')->exists($photoName)) {
            Storage::disk('public')->delete($photoName);
        }

        $this->contactService->deleteContactPhoto($contactId);

        flash('Foto excluida com sucesso!')->success();

        return redirect()->route('schedule.contacts.edit', $contactId);
    }


    public function exportContacts()
    {
        return Excel::download(new ContactsExport(), 'contacts.xlsx');
    }


    public function printPdf()
    {
        $userAuthId = auth()->user()->id;
        $contacts = $this->contactService->findAllContacts($userAuthId);
        $pdfContact = PDF::loadView('schedule.contacts.index_table_print', compact('contacts'));
        return $pdfContact->download('contatos.pdf');
    }
}
