<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestAddressEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'street' => 'required',
            'number' => 'required',
            'neighborhood' => 'required',
            'cep' => 'required',
            'city' => 'required',
            'state' => 'required'
        ];
    }

    public function messages()
    {
        return [
           'required' => 'O campo é obrigatório.',
        ];
    }
}
