<?php

namespace App\Jobs;

use App\User;
use stdClass;
use App\Mail\SendEmail;
use App\Models\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $contact;

    private $auth;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Contact $contact, stdClass $auth)
    {
        $this->contact = $contact;

        $this->auth = $auth;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
          
        Mail::send(new SendEmail($this->contact, $this->auth));
    }
}
