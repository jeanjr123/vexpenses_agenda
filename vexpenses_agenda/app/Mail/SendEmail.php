<?php

namespace App\Mail;

use App\User;
use stdClass;
use App\Models\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $contact;

    private $auth;

 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact, stdClass $auth)
    {
        $this->contact = $contact;

        $this->auth = $auth;

    

        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    //setar parametros passados
    public function build()
    {
        $this->subject('Novo Contato Agenda');
        $this->to($this->auth->email, $this->auth->name);

       

        return $this->markdown('schedule.emails.email', [
            'user' => $this->contact,
            'auth' => $this->auth,
          
        ]);
    }
}
