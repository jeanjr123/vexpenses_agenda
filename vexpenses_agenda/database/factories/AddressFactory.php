<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'contact_id' => 1,
        'street' => $faker->name,
        'number' => '123',
        'cep' => '14409112',
        'neighborhood' => $faker->sentence(1),
        'city' => $faker->sentence(1),
        'state' => $faker->sentence(1),
    ];
});
