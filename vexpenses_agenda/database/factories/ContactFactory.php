<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    return [

        'name' => $faker->name,
        'category_id' => 1,
        'email' => $faker->unique()->safeEmail,

    ];
});
