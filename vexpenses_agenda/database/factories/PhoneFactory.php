<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Phone;
use Faker\Generator as Faker;

$factory->define(Phone::class, function (Faker $faker) {
    return [
        'contact_id' => 1,
        'phone' => '163535-8512',
        'mobile_phone' => '1699222-8512',
    ];
});
