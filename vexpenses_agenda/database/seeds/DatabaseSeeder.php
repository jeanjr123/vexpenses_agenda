<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Models\Category::class, 10)->create();

       // $this->call(UsersTableSeeder::class);

        factory(App\User::class, 10)->create()->each(function ($user) {
        
                $user->contact()->save(factory(App\Models\Contact::class)->make())->each(function ($contact) {
    
                    $contact->phone()->save(factory(App\Models\Phone::class)->make());
                    $contact->address()->save(factory(App\Models\Address::class)->make());
                });
        });
    }
}
