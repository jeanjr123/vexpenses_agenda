<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Jobs\SendEmailJob;
use App\Mail\SendEmail;
use Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('schedule/')->name('schedule.')->namespace('Schedule')->group(function () {

    Route::resource('categories', 'CategoryController');

    Route::resource('phones', 'PhoneController');

    Route::resource('adresses', 'AddressController');

    Route::resource('contacts', 'ContactController');

    Route::post('photos/remove/{contactId}', 'ContactController@removePhoto')->name('photo.remove');

    Route::get('exports', 'ContactController@exportContacts')->name('contacts.export');

    Route::get('print', 'ContactController@printPdf')->name('contacts.print');

    Route::get('contactsByDate', 'ContactController@showContactByDate')->name('contacts.byDate');

   

});





